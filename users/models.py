from django.db import models
from django.contrib.auth.models import User

class tbProfiles(models.Model):
    class Meta:
        db_table = 'tbProfiles'

    user   = models.ForeignKey(User, on_delete=models.PROTECT)
    passwd = models.CharField(max_length = 64)

    address_zip = models.CharField(max_length = 256, default = 'none')
    address_home = models.CharField(max_length = 256)
    address_state = models.CharField(max_length = 256)
    address_country = models.CharField(max_length = 256)

    phone_mobile = models.CharField(max_length = 256)
    phone_home   = models.CharField(max_length = 256)
    phone_work   = models.CharField(max_length = 256)
