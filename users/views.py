from django.shortcuts import render
from django.shortcuts import redirect
from django.shortcuts import render_to_response
from django.template.loader import render_to_string
from django.http.response import HttpResponse
from django.http import JsonResponse
from django.middleware import csrf
from django.shortcuts import get_object_or_404
from django.template.loader import get_template
from django.template import Context
from django.contrib.auth.models import User
from django.contrib.sessions.models import Session
from django.contrib.auth.hashers import make_password
from django.contrib.auth import authenticate, login, logout

import hashlib
import uuid
from datetime import datetime, timedelta
from django.utils import timezone
#from .models import tbUsers, tbProfiles, tbSession, tbSessionClosed
from k13.logs import k13_log

def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip

def get_session_user(request):
    from django.conf import settings
    
    session_id = request.COOKIES.get('sessionid') 

    if (session_id is None):
        k13_log('K13_log: No session id found.')
        return None
    else:
        k13_log('K13_log: Session id is: ' + str(session_id))

    
    try:
        k13_log('K13_log: XXX 1 ' + str(type(request.user)))
        if not request.user.is_authenticated:
            raise Exception('User is not autheticated.');
        else:
            user = request.user
        k13_log('K13_log: XXX2')
            
        k13_log('K13_log: finding session')
       # session = request.session #tbSession.objects.get(sid=session_id, csrf=csrf.get_token(request))

        #if session is None:
        #    raise Exception('Invalid session')

        k13_log('K13_log: session found and is valid ')
        k13_log('K13_log: session found user is ' + user.email)

        return user
        
    except Exception as e:
        k13_log('K13_log: while check session error occured is ' + str(e))
        return None

    return None

    
def user_basic_menu(request):
    html = ''  
    
    k13_log('K13_log: checking session')
    user = get_session_user(request)
    
    if (user is None):
        k13_log('K13_log: Session not found user')
        html = get_template('login_form.xml').render({'csrf_token': csrf.get_token(request)})        
    else:
        html = get_template('user_base_menu.xml').render({'csrf_token': csrf.get_token(request), 'email': user.email})
        k13_log('K13_log: session found user is ' + user.email)

    return html

# Create your views here.
def basic(request):
    html = "default users page"
    return HttpResponse(html)

def profile(request):
    from k13.views import head_content
    from k13.views import main_panel
    
    user = get_session_user(request)
    
    if (user is None):
        return redirect('/blogs/')
    
    errorMessage = None
    html = render_to_string('profile.html', {'head_content': head_content('Profile'), 'main_panel': main_panel(request),
                                             'csrf_token': csrf.get_token(request), 'message': errorMessage, 'user': user})
    return HttpResponse(html)
    
def settings(request):
    html = "default user settings page"
    return HttpResponse(html)
    
def signin(request):
    from k13.views import head_content
    from k13.views import main_panel

    if request.method == 'POST':
        u_ip    = get_client_ip(request)
        u_mail  = request.POST.get('email')
        u_pass  = request.POST.get('password')

        v_signin = False
        v_sid    = ''
        message  = ''
	
        try:
            users = User.objects.all()

            k13_log('K13_log:  User pass for save: ' + make_password(u_pass))
            
            tmp = User.objects.get(email=u_mail)

            if tmp is None:
                raise 'User is not registered.'

            k13_log('K13_log:  Email: ' + u_mail + ' Pass: ' + u_pass)
            user = authenticate(request, username=tmp.username, email=u_mail, password=u_pass) 

            k13_log('K13_log:  User: authenticated ' + str(user))

            if user is None:
                k13_log('K13_log: User is not exist')
                raise User.DoesNotExist('User is invalid!')

            k13_log('K13_log:  User: check if active')
            
            if user.is_active:
                request.session.set_expiry(36000)
                login(request, user)

                if not request.session.exists(request.session.session_key):
                    request.session.create()
                    
            k13_log('K13_log:  Session key is ' + str(request.session.session_key))
            
            from django.db import transaction
            transaction.commit()

            #v_sid   = session.sid
            #html = render_to_string('signin_success.html', {})
            html = render_to_string('result.html', {'head_content': head_content('K13 Forum'), 'main_panel': '', 'csrf_token': csrf.get_token(request), \
                                                    'message': 'Success.', 'description': 'Your logged in.', \
                                                    'url': '/', 'delay': '2000'})

            response = HttpResponse(html)
            #response.set_cookie('k13_session_id', v_sid)
            k13_log('K13_log: successfully signed response')
        
            return response
        
        except Exception as e:
            k13_log('K13_log: user does not exist ' + str(e))
            message = 'user not exist'
        
        except Exception as e:
            message = str(e)
            k13_log('K13_log: ' + message)
    else:
        message = str('Invalid authentication method.')
        k13_log('K13_log: ' + message)

    from k13.views import basic as k13_basic
    
    #html = render_to_string('signin_fail.html', {'head_content': head_content('login'), 'main_panel': main_panel(request), 'csrf_token': csrf.get_token(request)})
    html = render_to_string('result.html', {'head_content': head_content('K13 Forum'), 'main_panel': '', 'csrf_token': csrf.get_token(request), \
                                            'message': 'Fail.', 'description': 'Unable to login as ' + message + '.', \
                                            'url': '/', 'delay': '2000'})

    return HttpResponse(html)
		
	
def signup(request):
    from k13.views import head_content
    from k13.views import main_panel
    
    isError = False
    errorMessage = ''
	
    if request.method == "POST":
        user_name = request.POST.get('first_name')
        user_surname = request.POST.get('last_name')
        user_email = request.POST.get('email')
        user_pass = request.POST.get('password')
        user_ppass = request.POST.get('password_confirmation')
        
        
        if  len(user_name) < 1 or len(user_surname) < 1 or \
            len(user_email) < 1 or len(user_pass) < 1 or \
            len(user_ppass) < 1:
            isError = True
            errorMessage = 'Error: Field is empty'
        elif user_pass != user_ppass:
            isError = True
            errorMessage = 'Error: Password confirmation is invalid.'
        else:
            user = None	
            try:
                user = User.objects.filter(email=user_email).first()
            except User.DoesNotExist:
              pass
            except Exception as e:
                k13_log('K13_log: ' + str(e))
                pass

            if user is not None:
                isError = True
                errorMessage = 'Error: Such email already registered.'

                errorMessage += ' ' + str(user)
                k13_log('K13_Log: ' + errorMessage)

            if isError:
                html = render_to_string('signup.html', {'head_content': head_content('Sign up'), 'main_panel': main_panel(request),                \
                                                        'csrf_token': csrf.get_token(request), 'message': errorMessage})
            else:
                #user = User(username=user_email, email=user_email, password=user_pass, first_name=user_name, last_name=user_surname)
                user = User.objects.create_user(user_name + ' ' + user_surname, user_email, user_pass)
                user.save()
                title = 'Sign up success'
                html = render_to_string('signup_success.html', {'head_content': head_content('Sign up success'), 'main_menu': main_panel(request), 'csrf_token': csrf.get_token(request)})

            return HttpResponse(html)
    
    html = render_to_string('signup.html', {'head_content': head_content('Sign up'), 'main_panel': main_panel(request), 'csrf_token': csrf.get_token(request), 'message': errorMessage})
    
    return HttpResponse(html)


def signout(request):
    from k13.views import head_content
    from k13.views import main_panel
    
    html = ''
    
    k13_log('K13_log: Logging out 1')
    try:
        if request.user.is_authenticated:
            logout(request)
            k13_log('K13_log: Logged out')
    except Exception as e:
        k13_log('K13_log: while check session error occured is ' + str(e))
        
    k13_log('K13_log: Logging out 2')
    #html = render_to_string('logout.html', {'head_content': head_content('logout'), 'main_panel': main_panel(request), 'csrf_token': csrf.get_token(request)})
    html = render_to_string('result.html', {'head_content': head_content('K13 Forum'), 'main_panel': '', 'csrf_token': csrf.get_token(request), \
                                            'message': 'Success.', 'description': 'Your logged out.', \
                                            'url': '/', 'delay': '2000'})
    k13_log('K13_log: Logging out 3')

    response = HttpResponse(html)

    k13_log('K13_log: Logging out 4')
    
    return response

    
def newname(request):
    fname = request.GET.get('fname', None)
    lname = request.GET.get('lname', None)

    user = get_session_user(request)

    data = {'Fail': 'Unable update user name data.'}

    try:
        if user is not None:
            user.first_name = fname
            user.last_name = lname
            user.save()
            data = {'Success': 'User name data updated.'}
    except Exception as e:
        data = {'Fail': 'Unable update user name as ' + str(e) + '.'}
    
    return JsonResponse(data)

def newpass(request):
    passwd = request.POST.get('pass', None)

    data = {'Fail': 'Unable update user password.'}

    user = get_session_user(request)
    
    try:
        if user is not None:
            user.set_password(passwd)
            user.save()
            data = {'Success': 'User password updated.'}
        else:
            data = {'Fail': 'Unable update user password as cannot get session user.'}
    except Exception as e:
        data = {'Fail': 'Unable update user password as have error: ' + str(e) + '.'}
    
    return JsonResponse(data)
