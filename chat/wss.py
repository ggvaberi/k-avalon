import socket, threading, time, select

_wss = None

class ChatWss(threading.Thread):
    #Web socket chat
    
    def __init__(self, port):
        threading.Thread.__init__(self)
        self.active = True
        self.server = None
        self.clients = ()
        self.open(port)
    
    def open(self, port):
        self.server = socket.socket()
        self.server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.server.bind(('', port))
        self.server.listen(10)
        self.server.setblocking(0)

    def close(self):
        self.active = False
    
    def run(self):
        while(self.active):
            try:
                c = self.server.accept()
                c.setblocking(0)
                self.clients.append(c)
            except Exception as e:
                pass

            rs, ws, es = select.select(self.client, None, None, 5000)

            for r in rs:
                self.reader(r)

        for c in self.clients:
            c.close()

        self.server.close()

    def reader(self, c):
        pass

def start_chat_wss(port):
    if (_wss is not None):
        return

    _wss = ChatWss(port)

def stop_chat_wss():
    if (_wss is None):
        return

    _wss.close()
    _wss.join()
