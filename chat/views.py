from django.shortcuts import render
from django.template.loader import render_to_string
from django.http.response import HttpResponse
from django.middleware import csrf
from django.core.paginator import Paginator
from django.contrib.auth.models import User
from blogs.models import tbBlogs, tbBlogComments, tbBlogSections
from k13.logs import k13_log

def index(request, page=1):
    from k13.views import main_panel
    from k13.views import head_content
    from users.views import get_session_user

    user = get_session_user(request)

    html = render_to_string('chat.html', {'head_content': head_content('K13 Blogs'), 'main_panel': main_panel(request), 'user': user})
    return HttpResponse(html)


def new_session(request):
    pass
