from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class tbChat(models.Model):
    class Meta:
        db_table = 'tbChat'

    user    = models.ForeignKey(User, on_delete=models.PROTECT)

    date    = models.DateTimeField(auto_now_add=True)
    expire  = models.DateTimeField()
    
    title   = models.CharField(max_length = 256)
    passwd  = models.CharField(max_length = 256)
    invite  = models.CharField(max_length = 128)

    maximal = models.IntegerField(default = 5)

class tbChatClosed(models.Model):
    class Meta:
        db_table = 'tbChatClosed'

    user    = models.ForeignKey(User, on_delete=models.PROTECT)

    date    = models.DateTimeField()
    expire  = models.DateTimeField()
    
    title   = models.CharField(max_length = 256)
    passwd  = models.CharField(max_length = 256)
    invite  = models.CharField(max_length = 128)

    maximal = models.IntegerField(default = 5)

    expired = models.BooleanField(default=True)
