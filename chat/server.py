import websockets

from k13.logs import k13_log

__server = None
__port   = 7866

def hello(websocket, path):
    name = yield websocket.recv()
    print("< {}".format(name))

    greeting = "Hello {}!".format(name)
    yield websocket.send(greeting)
    print("> {}".format(greeting))


def start():
    global __server
    global __port

    if __server is not None:
        k13_log("chat server already running.")
        return False

    __server = websockets.serve(hello, 'localhost', __port)

    return True

def stop():
    global __server

    if (__server is None):
        k13_log("chat server already stopped.")
        return False

    __server.close()
    __server.wait_closed()

    __server = None

    return True


