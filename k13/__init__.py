import threading
import time
import signal
from datetime import datetime, timedelta

'''
from users.models import tbSession, tbSessionClosed
from .logs import k13_log

#import chat.server as chat_server

session_service_running = False
session_service_timeout = 10
session_service_thread  = None

def session_service():
    global session_service_running
    
    k13_log('K13_log: session service start.')

    if session_service_running is False:
        session_service_running = True

    while session_service_running is True:
        try:
            sessions = tbSession.objects.all()

            for session in sessions:
                delta_update  = datetime.now(session.update.tzinfo) - session.update
                delta_timeout = datetime.now(session.start.tzinfo) - session.start

                if delta_update.total_seconds() > settings.K13_SESSION_UPDATE or \
                   delta_timeout.total_seconds() > settings.K13_SESSION_TIMEOUT:
                    k13_log('Closing session ' + session.sid)
                    c_session       = tbSessionClosed()
                    c_session.uid   = session.uid
                    c_session.uip   = session.uip
                    c_session.sid   = session.sid
                    c_session.csrf  = session.csrf
                    c_session.start = session.start
                    c_session.save()
                    session.delete()
        except Exception as e:
            k13_log('K13_log: while check session error occured is ' + str(e))

        time.sleep(session_service_timeout)

    k13_log('K13_log: session service stops.')


def sigterm_handler():
    global session_service_running
    
    if session_service_thread:
        session_service_running = False
        session_service_thread.join()

#    chat_server.stop()
'''

#session_service_thread = threading.Thread(target=session_service)
#session_service_thread.start()
#chat_server.start()
