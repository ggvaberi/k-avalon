from django.conf.urls import include, url
from django.contrib import admin

from . import views

admin.autodiscover()

urlpatterns = [
    #    url(r'^admin/', include('admin.site.urls')),
    url(r'^users/', include('users.urls')),
    url(r'^news/', include('news.urls')),
    url(r'^chat/', include('chat.urls')),
    #    url(r'^mail/', include('mail.urls')),
    #    url(r'^shop/', include('shop.urls')),
    url(r'^forum/', include('forum.urls')),
    url(r'^weather/', include('weather.urls')),
    url(r'^blogs/',   include('blogs.urls')),
    url(r'^$',        views.basic),
]
