from django.shortcuts import render
from django.shortcuts import render_to_response
from django.shortcuts import redirect
from django.template.loader import render_to_string
from django.template.loader import get_template
from django.template import Context
from django.http.response import HttpResponse
from django.middleware import csrf
from k13.logs import k13_log

# Create your views here.
def basic(request):
  from users.views import user_basic_menu
	  
  html = render_to_string('k13.html', {'head_content': head_content('K13'), 'main_panel': main_panel(request)})
  
  return HttpResponse(html)


def main_panel(request):
  from users.views import user_basic_menu

  main_menu    = render_to_string('main_menu.xml')
  user_menu    = user_basic_menu(request)
  
  return render_to_string('main_panel.xml', {'user_menu': user_menu, 'main_menu': main_menu})

def head_content(title):  
  return render_to_string('head_content.xml', {'title': title})
