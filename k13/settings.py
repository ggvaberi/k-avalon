"""
Django settings for k13 project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
#BASE_DIR = os.path.dirname(os.path.dirname(__file__))
print("DEBUG: BASE_FILE is " + __file__)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
PROJECT_DIR = os.getcwd()

print("DEBUG: BASE_DIR is " + BASE_DIR)

SETTINGS_PATH = os.path.dirname(os.path.dirname(__file__))

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'p2u+0t^=!q&8**sc6y1&(c8_se+%b#d*f%m+ff&4t2@6-_fr15'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = ['*']

DATA_UPLOAD_MAX_MEMORY_SIZE = 5242880

#Check if on heroku.

is_heroku = None

is_heroku = os.environ.get('IS_HEROKU', None)

# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
#    'admin'
    'users',
    'shop',
    'chat',
    'forum',
    'news',
    'blogs',
]

#MIDDLEWARE_CLASSES
MIDDLEWARE = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.security.SecurityMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'whitenoise.middleware.WhiteNoiseMiddleware',
)

# Authentication backends
AUTHENTICATION_BACKENDS = (
        'django.contrib.auth.backends.ModelBackend', # default any other authentication backends
    )

ROOT_URLCONF = 'k13.urls'

WSGI_APPLICATION = 'k13.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db_k13.sqlite3'),
    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/

#STATIC_URL = PROJECT_DIR + '/static/'
#STATIC_ROOT      = ''
STATIC_ROOT      = os.path.join(BASE_DIR, 'staticfiles')
STATIC_URL       = '/static/'
STATICFILES_DIRS = [ os.path.join(BASE_DIR, 'static/') ]

if is_heroku:
    STATICFILES_DIRS = [ os.path.join(BASE_DIR, '../static/') ]

STATICFILES_STORAGE = 'django.contrib.staticfiles.storage.ManifestStaticFilesStorage'

print("DEBUG: STATIC_ROOT is " + str(STATIC_ROOT))
print("DEBUG: STATIC_URL is " + str(STATIC_URL))
print("DEBUG: STATICFILES_DIRS is " + str(STATICFILES_DIRS))

#Templates
#TEMPLATE_CONTEXT_PROCESSORS = (
#  "django.core.context_processors.static",
#)

TEMPLATE_DIRS = (
    os.path.join(BASE_DIR,  'static/html'),
)

print("DEBUG: TEMPLATE_DIRS is " + str(TEMPLATE_DIRS))

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(SETTINGS_PATH, 'static/html')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

K13_SESSION_UPDATE  = (30  * 60)
K13_SESSION_TIMEOUT = (120 * 60)

K13_WEATHER_APP_ID = 'a3bd0ffa97f1414465e041af5acc3323';
