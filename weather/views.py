from django.shortcuts import render
from django.template.loader import render_to_string
from django.http.response import HttpResponse
from django.http import JsonResponse
from django.conf import settings
from datetime import datetime  
from datetime import timedelta  
import calendar

import requests
import json

from k13.logs import k13_log

def basic(request):
    from k13.views import main_panel
    from k13.views import head_content

    location = get_location(request)
    city     = get_city(request)

    html = render_to_string('weather.html', {'head_content': head_content('K13 Weather'), 'main_panel': main_panel(request), \
                                             'lat': location[0], 'lon': location[1], 'city': city})
    return HttpResponse(html)

def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip

def get_location(request):
    send_url = 'https://geoip-db.com/json/' + get_client_ip(request)

    lat = lon = 0
    
    try:
        print('DEBUG: get_location.send_url is ' + send_url)
        r = requests.get(send_url)
        j = json.loads(r.text)
        print('DEBUG: get_location.r_text is ' + r.text)
        lat = j['latitude']
        lon = j['longitude']
    except Exception as e:
        k13_log('get_location error is ' + str(e))
        print('ERROR: get_location error is ' + str(e))
        lat = lon = 1
        
    return (lat, lon)

def get_city(request):
    city = "London"
    ip = get_client_ip(request)   
    #ip = "188.121.195.219"

    try:
        r = requests.get('http://api.openweathermap.org/data/2.5/weather?q=London&appid=' + settings.K13_WEATHER_APP_ID)
        print('DEBUG: get_city location ' + str(r.json()))
    
        print('DEBUG: get_city forming url')
        url = 'https://geoip-db.com/json/'+ip
        r = requests.get(url)
        js = r.json()
        print('DEBUG: get_city json ' + str(js))

        if len(js['city']) > 0:
            city = js['city']
    except Exception as e:    
        k13_log('get_city error is ' + str(e))

    if (city is None or city == 'Not found'):
        city = 'London';
    
    return city

def getDate(i):
    date = datetime.now() + timedelta(days=i)
    return date.date()

def getDay(i):
    date = datetime.now() + timedelta(days=i)
    return calendar.day_name[date.weekday()]
    
def city(request):

    print('DEBUG: weather.city ...')
    
    city = request.GET.get('city', None)
    units = request.GET.get('type', None)

    if city is None or city == "":
        city = 'London'

    if units is None or units == "":
        units = 'metric'

    if units == 'F':
        units = 'imperial';
    elif units == 'C':
        units = 'metric';
        
    data = {}
    #    'is_taken': User.objects.filter(username__iexact=username).exists()
    #}
    #return JsonResponse(data)

    r_data = 'http://api.openweathermap.org/data/2.5/forecast?q=' + city + '&appid=a3bd0ffa97f1414465e041af5acc3323&units=' + units;

    try:
        #r = requests.get('http://api.openweathermap.org/data/2.5/weather?q=London&appid=a3bd0ffa97f1414465e041af5acc3323')
        r = requests.get(r_data)
        w_son = r.json()
        #print('DEBUG: get_city location ' + str(r.json()))
        #print('DEBUG: get_city location ' + str(w_son['list'][0]))

        for i in range(4):
            print('DEBUG: city tempera ' + str(w_son['list'][i]['main']))
            print('DEBUG: city weather ' + str(w_son['list'][i]['weather']))
            data[i] = {'temp': w_son['list'][i]['main']['temp'],
                       'weather': w_son['list'][i]['weather'][0]['main'],
                       'date': getDate(i),
                       'day': getDay(i),
                       'code':  w_son['list'][i]['weather'][0]['icon']}
        
    except Exception as e:    
        k13_log('get_city error is ' + str(e))
        data = {'Fail': 'Unable get weather data!!!'}
    
    return JsonResponse(data)
