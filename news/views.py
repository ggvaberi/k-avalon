import re
import requests
import xml.etree.ElementTree as ET
from xml import etree
from html.parser import HTMLParser
from django.shortcuts import render
from django.template.loader import render_to_string
from django.http.response import HttpResponse
from django.http import JsonResponse

from django.contrib.auth.models import User

from k13.logs import k13_log

from k13.views import main_panel
from k13.views import head_content
from users.views import get_session_user

#Currenty use buzzfeed
# Create your views here.
def index(request):
    req = requests.get('https://www.buzzfeed.com/world.xml')
    print('News request to buzzfeed ' + str(req))
    print('News request to buzzfeed ' + str(req.content))
    xml = ET.fromstring(req.content)

    html = 'Under construction!'

    news = []

    for item in xml.getiterator('item'):
        description = ''

        try:
            cdata = str(item.find('description').text)
            description = re.findall("<h1>(.*)</h1>", cdata)[0]
            print('description is ' + description)
        except Exception as e:
            print('Error ' + str(e))

        news.append({
            'description': description,
            'title': item.find('title').text,
            'link': item.find('link').text,
            'date': item.find('pubDate').text,
        })

    user_valid = False

    if get_session_user(request) is not None:
        user_valid = True

    r_users = User.objects.all()
    users = {}

    for user in r_users:
        id    = user.id
        uname = user.email.split('&')[0]
        users[id] = uname

    html = render_to_string('news.html', {'head_content': head_content('K13 News'), 'main_panel': main_panel(request), 'user_valid': user_valid, \
        'news': news})

    return HttpResponse(html)

def actual(request):
    req = requests.get('https://www.buzzfeed.com/world.xml')
    print('News request to buzzfeed ' + str(req))
    print('News request to buzzfeed ' + str(req.content))

    xml = ET.fromstring(req.content)

    ns = {'atom' : "http://www.w3.org/2005/Atom", 'content' : "http://purl.org/rss/1.0/modules/content/", 'dc' : "http://purl.org/dc/elements/1.1/",
          'media' : "http://search.yahoo.com/mrss/", 'snf' : "http://www.smartnews.be/snf"}

    news = {}

    index = 0

    for item in xml.getiterator('item'):
        description = ''
        imgurl = ''

        try:
            cdata = str(item.find('description').text)
            description = re.findall("<h1>(.*)</h1>", cdata)[0]
            print('description is ' + description)
        except Exception as e:
            print('Error ' + str(e))

        news[index] = {
            'description': description,
            'title': item.find('title').text,
            'link': item.find('link').text,
            'date': item.find('pubDate').text,
            'author': item.find("dc:creator", ns).text,
            'img': item.find('media:thumbnail', ns).attrib['url'],
        }

        index += 1

    return JsonResponse(news)
