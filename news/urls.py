from django.conf.urls import include, url

from . import views

urlpatterns = [
    url(r'actual', views.actual),
    url(r'^$', views.index),
]
