from django.conf.urls import include, url
from . import views

urlpatterns = [
                       
    url(r'^blog/(\d+)/', views.blog),
    url(r'^add/', views.add),
    url(r'^publish/', views.publish),
    url(r'^comment/', views.comment),
    url(r'^list/', views.mylist),
    url(r'^remove/(\d+)/', views.remove),
    url(r'^(\d+)/', views.index),
    url(r'^$', views.index),
]
