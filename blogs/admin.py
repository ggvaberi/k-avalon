from django.contrib import admin
from blogs.models import tbBlogs, tbBlogSections, tbBlogComments

# Register your models here.
admin.site.register(tbBlogs)
admin.site.register(tbBlogSections)
admin.site.register(tbBlogComments)
