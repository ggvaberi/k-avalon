from django.shortcuts import render
from django.shortcuts import redirect
from django.template.loader import render_to_string
from django.http.response import HttpResponse
from django.middleware import csrf
from django.core.paginator import Paginator
from django.contrib.auth.models import User

from blogs.models import tbBlogs, tbBlogComments, tbBlogSections
from k13.logs import k13_log

from k13.views import main_panel
from k13.views import head_content
from users.views import get_session_user

def index(request, page=1):
    user_valid = False

    if get_session_user(request) is not None:
        user_valid = True

    r_users = User.objects.all()
    users = {}

    for user in r_users:
        id    = user.id
        uname = user.email.split('&')[0]
        users[id] = uname

    blogs = tbBlogs.objects.all()
    c_page = Paginator(blogs, 5)
    html = render_to_string('blogs.html', {'head_content': head_content('K13 Blogs'), 'main_panel': main_panel(request), 'user_valid': user_valid, \
                                           'blogs': c_page.page(page), 'users': users})

    return HttpResponse(html)

def add(request):
    from k13.views import main_panel
    from k13.views import head_content

    sections = None
    base_section = None

    user = get_session_user(request)

    if user is None:
        return redirect('/blogs/')
    else:
        k13_log('Adding blog user: ' + user.username)
        
    try:
        sections     = tbBlogSections.objects.all()
        base_section = sections[0].section
    except Exception as e:

        pass

    html = render_to_string('add_blog.html', {'head_content': head_content('K13 Blog'), 'main_panel': main_panel(request), \
                                              'csrf_token': csrf.get_token(request), 'sections': sections, 'base_section': base_section})

    return HttpResponse(html)

def publish(request):
    from users.views import get_session_user
    from k13.views import main_panel
    from k13.views import head_content

    message = ''

    k13_log('try to publish')

    try:
        user = get_session_user(request)

        if user is None:
            raise Exception('Not valid session.')

        k13_log('user is ' + user.username)

        title     = request.POST.get('title')
        content   = request.POST.get('content')
        c_section = request.POST.get('section')
        image       = request.POST.get('image')
        description = request.POST.get('description')
 
        if len(title) < 1 or len(content) < 1:
            raise Exception('Some of field is empty.')

        k13_log('user: ' + user.username + ' title: ' + title + ' section: ' + c_section)

        k13_log('user: creating blog')
        blog = tbBlogs.objects.create(user=user, title=title, content=content, section=tbBlogSections.objects.get(section=c_section), image=image, description=description)
        b_id = blog.pk

        blog.save();

        k13_log('user: blog saved ' + str(b_id))

        html = render_to_string('blog_publish.html', {'head_content': head_content('K13 Blog'), 'main_panel': main_panel(request), \
                                                      'csrf_token': csrf.get_token(request), 'bid': b_id})

    except Exception as e:
        message = 'Error: ' + str(e)

        from k13.views import main_panel
        from k13.views import head_content

        sections     = None
        base_section = None

        try:
            sections     = tbBlogSections.objects.all()
            base_section = sections[0].section
        except Exception as e:
            message += " [" + str(e) + "]"

        html = render_to_string('add_blog.html', {'head_content': head_content('K13 Blog'), 'main_panel': main_panel(request), \
                                                  'csrf_token': csrf.get_token(request), 'sections': sections, 'base_section': base_section,
                                                  'message': message})

    return HttpResponse(html)

def blog(request, bid=-1):
    from users.views import get_session_user
    from k13.views import main_panel
    from k13.views import head_content

    comments = None
    c_blog   = None
    owner = False

    s_user   = get_session_user(request)

    try:
        c_blog = tbBlogs.objects.get(id=bid)
        comments = tbBlogComments.objects.filter(blog=c_blog)

        k13_log('blog error: user ' + str(s_user.id) + ' blog owner: ' + str(c_blog.user_id));
        
        if s_user.id == c_blog.user_id:
            owner = True
            
    except Exception as e:
        k13_log('blog error: ' + str(e));

    html = render_to_string('blog.html', {'head_content': head_content('K13 Blog'), 'main_panel': main_panel(request), \
                                          'csrf_token': csrf.get_token(request), 'blog': c_blog, 'comments': comments, 'user': s_user, 'owner': owner})
    return HttpResponse(html)

def comment(request):
    from users.views import get_session_user
    k13_log('got comment request...')

    bid = -1
    try:
        user = get_session_user(request)

        if user is None:
            raise Exception('No session user')

        bid  = request.POST.get('bid')
        text = request.POST.get('comment')

        if bid is None or int(bid) < 0:
            raise Exception('Invalid blog id')

        if len(text) < 8:
            raise Exception('Comment content less then 8 characters')

        comment = tbBlogComments()
        comment.user = user
        comment.blog = tbBlogs.objects.get(id=bid)
        comment.content = text
        comment.save()

    except Exception as e:
        k13_log('blogs.views.comment: ' + str(e))
        html = render_to_string('result.html', {'head_content': head_content('K13'), 'main_panel': '', 'csrf_token': csrf.get_token(request), \
                                                'message': 'Failed.', 'description': str(e) + '.', \
                                                'url': '/blogs/blog/' + str(bid), 'delay': '2000'})
        return HttpResponse(html)

    return blog(request, bid)

def mylist(request):
    from users.views import get_session_user
    k13_log('got comment request...')

    try:
        user = get_session_user(request)

        if user is None:
            return redirect('/')

        blogs = tbBlogs.objects.filter(user_id=user)

    except Exception as e:
        k13_log('blogs.viws.comment: ' + str(e))
        return redirect('/')

    html = render_to_string('myblogs.html', {'head_content': head_content('K13 Blogs'), 'main_panel': main_panel(request), 'blogs': blogs})

    return HttpResponse(html)

def remove(request, bid=-1):
    from users.views import get_session_user
    from k13.views import main_panel
    from k13.views import head_content

    comments = None
    c_blog   = None
    owner = False

    s_user   = get_session_user(request)

    html = None

    try:
        c_blog = tbBlogs.objects.get(id=bid)
        comments = tbBlogComments.objects.filter(blog=c_blog)

        k13_log('blog remove: user ' + str(s_user.id) + ' blog owner: ' + str(c_blog.user_id) + ' bid: ' + str(bid));
        
        if s_user.id == c_blog.user_id:
            c_blog.delete()
            k13_log('blog removed')
            html = render_to_string('result.html', {'head_content': head_content('K13'), 'main_panel': '', 'csrf_token': csrf.get_token(request), \
                                                    'message': 'Success.', 'description': 'Blog removed.', \
                                                    'url': '/blogs/list/', 'delay': '2000'})
            k13_log('blog remove content generated.')

        else:
            raise Exception('Not valid user.')
            
    except Exception as e:
        html = render_to_string('result.html', {'head_content': head_content('K13'), 'main_panel': '', 'csrf_token': csrf.get_token(request), \
                                                'message': 'Failed.', 'description': 'Cannot delete blog as ' + str(e) + '.', \
                                                'url': '/blogs/list/', 'delay': '2000'})

    return HttpResponse(html)

