from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class tbBlogSections(models.Model):
    class Meta:
        db_table = 'tbBlogSections'

    section = models.CharField(max_length = 256)

class tbBlogs(models.Model):
    class Meta:
        db_table = 'tbBlogs'

    user    = models.ForeignKey(User, on_delete=models.PROTECT)

    date        = models.DateTimeField(auto_now_add=True) 
    title       = models.CharField(max_length = 256)
    section     = models.ForeignKey(tbBlogSections, on_delete=models.PROTECT)
    content     = models.TextField()
    description = models.CharField(max_length = 1024)
    image       = models.TextField()

class tbBlogsRemoved(models.Model):
    class Meta:
        db_table = 'tbBlogsRemoved'

    user    = models.ForeignKey(User, on_delete=models.PROTECT)

    date        = models.DateTimeField(auto_now_add=True) 
    title       = models.CharField(max_length = 256)
    section     = models.ForeignKey(tbBlogSections, on_delete=models.PROTECT)
    content     = models.TextField()
    description = models.CharField(max_length = 1024)
    image       = models.TextField()

class tbBlogComments(models.Model):
    class Meta:
        db_table = 'tbBlogComments'

    blog    = models.ForeignKey(tbBlogs, on_delete=models.PROTECT);
    user    = models.ForeignKey(User, on_delete=models.PROTECT)
    content = models.TextField()
    date    = models.DateTimeField(auto_now_add=True) 
    
