from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class tbForum(models.Model):
    class Meta:
        db_table = 'tbForum'

    title   = models.CharField(max_length = 256)
    date    = models.DateTimeField(auto_now_add=True)

class tbForumTheme(models.Model):
    class Meta:
        db_table = 'tbForumTheme'

    forum = models.ForeignKey(tbForum, on_delete=models.PROTECT)
    user  = models.ForeignKey(User, on_delete=models.PROTECT)

    title      = models.CharField(max_length = 256)
    decription = models.CharField(max_length = 512)
    content    = models.TextField()
    date       = models.DateTimeField(auto_now_add=True)

class tbForumComment(models.Model):
    class Meta:
        db_table = 'tbForumComment'

    theme = models.ForeignKey(tbForumTheme, on_delete=models.PROTECT)
    user  = models.ForeignKey(User, on_delete=models.PROTECT)

    comment = models.TextField()
    date    = models.DateTimeField(auto_now_add=True)
