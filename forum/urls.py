from django.conf.urls import include, url
from . import views

urlpatterns = [
                       
    url(r'^section/(\d+)/',     views.section),
    url(r'^theme/new/(\d+)/',   views.new_theme),
    url(r'^theme/publish/',     views.publish_theme),
    url(r'^theme/comment/',     views.publish_comment),
    url(r'^theme/(\d+)/(\d+)/', views.theme),
    url(r'^$',                  views.index),
]
