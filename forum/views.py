from django.shortcuts import render
from django.template.loader import render_to_string
from django.http.response import HttpResponse
from django.middleware import csrf
from django.core.paginator import Paginator
from django.contrib.auth.models import User
from forum.models import tbForum, tbForumTheme, tbForumComment
from k13.logs import k13_log

# Create your views here.
def index(request):
    from k13.views import main_panel
    from k13.views import head_content
    from users.views import get_session_user


    user = get_session_user(request)

    if get_session_user(request) is not None:
        user_valid = True

    forums = None
    
    try:
        forums = tbForum.objects.all()
    except:
        k13_log('Forum index request fails.')

    html = render_to_string('forum.html', {'head_content': head_content('K13 Forum'), 'main_panel': main_panel(request), 'user': user, \
                                           'forums': forums})

    return HttpResponse(html)

def section(request, section_id=-1, page=1):
    from k13.views import main_panel
    from k13.views import head_content
    from users.views import get_session_user

    users = {}
    themes = None
    c_page = None
    section = None

    user = get_session_user(request)

    k13_log("Forum section info: session user is " + str(user));

    try:    
        section = tbForum.objects.get(id=section_id)
        themes = tbForumTheme.objects.filter(forum_id=int(section_id))
        
        c_page = Paginator(themes, 5)
        c_page = c_page.page(page)

    except Exception as e:
        k13_log("Forum section error: " + str(e))
    
    html = render_to_string('forum_section.html', {'head_content': head_content('K13 Blogs'), 'main_panel': main_panel(request), 'user': user, \
                            'themes': c_page, 'section': section})

    return HttpResponse(html)

def theme(request, theme_id=-1, page=1):
    from k13.views import main_panel
    from k13.views import head_content
    from users.views import get_session_user

    user = get_session_user(request)

    theme    = None
    c_page   = None
    comments = None

    try:
        theme    = tbForumTheme.objects.get(id=theme_id)
        comments = tbForumComment.objects.filter(theme=theme)
        c_page = Paginator(comments, 5)
        comments = c_page.page(page)
        
    except Exception as e:
        k13_log("Forum theme error is: " + str(e))

        
    html = render_to_string('forum_theme.html', {'head_content': head_content('K13 Forum'), 'main_panel': main_panel(request), 'user': user, \
                            'csrf_token': csrf.get_token(request), 'theme': theme, 'comments': comments})

    return HttpResponse(html)


def new_theme(request, section_id=-1):
    from k13.views import main_panel
    from k13.views import head_content
    from users.views import get_session_user

    user    = None
    themes  = None
    c_page  = None
    section = None
    message = None

    try:    
        section = tbForum.objects.get(id=section_id)

        if not user:
            raise Exception("invalid section");
        
        user = get_session_user(request)
        
        if not user:
            raise Exception("invalid user");
        
    except Exception as e:
        k13_log("Forum new theme error: " + str(e))
    
    html = render_to_string('forum_theme_new.html', {'head_content': head_content('K13 Blogs'), 'main_panel': main_panel(request), 'user': user, \
                            'csrf_token': csrf.get_token(request), 'section': section})

    return HttpResponse(html)
    
def publish_theme(request):
    from k13.views import main_panel
    from k13.views import head_content
    from users.views import get_session_user

    html    = None
    user    = None
    title   = None
    content = None
    section = None
    message = None

    try:    
        user = get_session_user(request)

        if not user:
            raise Exception('invalid user')
        
        title   = request.POST.get('title')
        content = request.POST.get('content')
        section = request.POST.get('section')

        section = tbForum.objects.get(id=section)

        if len(title) < 1 or len(content) < 1:
            raise Exception('Some of field is empty')

        theme         = tbForumTheme()
        theme.user    = user
        theme.title   = title
        theme.content = content
        theme.forum   = section

        theme.save()
        k13_log("Forum publish theme saved!")

        
        html = render_to_string('result.html', {'head_content': head_content('K13 Forum'), 'main_panel': main_panel(request), 'user': user, \
                                                'message': 'Success.', 'description': 'New theme added succefully.', \
                                                'url': '/forum/section/' + str(section.id) + '/', 'delay': '5000'})
        
    except Exception as e:
        message = str(e)
        k13_log("Forum publish theme error: " + message)
    
        html = render_to_string('forum_theme_new.html', {'head_content': head_content('K13 Forum'), 'main_panel': main_panel(request), 'user': user, \
                                            'csrf_token': csrf.get_token(request), 'section': section, 'message': message})

    return HttpResponse(html)

def publish_comment(request):
    from k13.views import main_panel
    from k13.views import head_content
    from users.views import get_session_user

    html    = None
    user    = None
    theme   = None
    comment = None
    message = None

    theme_id = -1

    try:    
        user = get_session_user(request)

        if not user:
            raise Exception('Invalid user')

        theme_id = request.POST.get('tid')
        theme = tbForumTheme.objects.get(id=theme_id)
        
        if not theme:
            raise Exception('Theme is not valid')
        
        content = request.POST.get('comment')

        if len(content) < 1:
            raise Exception('Comment field is empty')



        comment = tbForumComment()
        comment.user = user
        comment.theme = theme
        comment.comment = content
        comment.save()
        
        html = render_to_string('result.html', {'head_content': head_content('K13 Forum'), 'main_panel': main_panel(request), 'user': user, \
                                                'message': 'Success.', 'description': 'Your comment added succefully.', \
                                                'url': '/forum/theme/' + str(theme.id) + '/1/', 'delay': '3000'})
        
    except Exception as e:
        message = str(e)
        k13_log("Forum publish theme error: " + message)
    
        html = render_to_string('result.html', {'head_content': head_content('K13 Forum'), 'main_panel': main_panel(request), 'user': user, \
                                                'message': 'Fail.', 'description': message + '.', \
                                                'url': '/forum/theme/' + str(theme_id) + '/1/', 'delay': '3000'})

    return HttpResponse(html)
