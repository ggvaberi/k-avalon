#!/bin/bash

FSCK=/tmp/k13.sck
FPID=/tmp/k13.pid
FLOG=/tmp/k13.log

function start {
    #python manage.py runfcgi method=prefork socket=$FSCK pidfile=$FPID &
    python manage.py runserver 127.0.0.1:8000 > $FLOG 2>&1 &
    sleep 3
    #chmod a+rw $FSCK
}

function stop {
    #kill `cat $FPID`
    fuser -k 8000/tcp
    rm -f $FPID
    #rm -f $FSCK
}


case $1 in 
    start)
	start
	;;
    stop)
	stop
	;;
    restart)
	stop
	start
	;;
    *)
	echo "Wrong command!"
	exit
esac

